//package top.codef.codef.feign;
//
//import feign.Body;
//import feign.Headers;
//import feign.RequestLine;
//import top.codef.codef.entities.ProjectState;
//
//public interface CodefClientFeign {
//
//	@RequestLine("POST /serviceCheck/state")
//	@Headers("Content-Type: application/json")
//	@Body("{projectState}")
//	public String serviceCheck(ProjectState projectState);
//}
