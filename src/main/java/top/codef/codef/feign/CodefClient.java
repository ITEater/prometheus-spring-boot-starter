//package top.codef.codef.feign;
//
//import java.io.IOException;
//import java.lang.reflect.Type;
//import java.nio.charset.StandardCharsets;
//
//import org.apache.commons.codec.digest.DigestUtils;
//import org.apache.commons.lang3.RandomStringUtils;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//import feign.Feign;
//import feign.FeignException;
//import feign.RequestTemplate;
//import feign.Response;
//import feign.codec.DecodeException;
//import feign.codec.Decoder;
//import feign.codec.EncodeException;
//import feign.codec.Encoder;
//import feign.codec.ErrorDecoder;
//import feign.slf4j.Slf4jLogger;
//import top.codef.codef.components.CodefHeader;
//import top.codef.codef.entities.ProjectState;
//import top.codef.codef.properties.PrometheusCodefCloudProperties;
//import top.codef.crypt.aes.AESUtils;
//import top.codef.crypt.aes.AesAlgorithm;
//import top.codef.exceptions.PrometheusException;
//
//public class CodefClient implements ServiceStateNotice {
//
//	private final PrometheusCodefCloudProperties prometheusCodefCloudProperties;
//
//	private final CodefClientFeign codefClientFeign;
//
//	private final ObjectMapper objectMapper;
//
//	/**
//	 * @param prometheusCodefCloudProperties
//	 * @param objectMapper
//	 */
//	public CodefClient(PrometheusCodefCloudProperties prometheusCodefCloudProperties, ObjectMapper objectMapper) {
//		this.prometheusCodefCloudProperties = prometheusCodefCloudProperties;
//		this.objectMapper = objectMapper;
//		codefClientFeign = Feign.builder().encoder(new CodefEncoder()).decoder(new CodefDecoder())
//				.errorDecoder(new CodefErrorDecoder()).logger(new Slf4jLogger(CodefClient.class))
//				.logLevel(prometheusCodefCloudProperties.getLogLevel())
//				.target(CodefClientFeign.class, prometheusCodefCloudProperties.getUrl());
//	}
//
//	@Override
//	public void notice(ProjectState projectState) {
//		codefClientFeign.serviceCheck(projectState);
//	}
//
//	public class CodefEncoder implements Encoder {
//
//		@Override
//		public void encode(Object object, Type bodyType, RequestTemplate template) throws EncodeException {
//			try {
//				byte[] body = objectMapper.writeValueAsBytes(object);
//				System.out.println(new String(body, StandardCharsets.UTF_8));
//				String nonce = RandomStringUtils.random(16, true, true);
//				String bodySMd5 = DigestUtils.md5Hex(body);
//				System.out.println(bodySMd5);
//				String timestamp = String.valueOf(System.currentTimeMillis());
//				String authentication = AESUtils.encrypt(AesAlgorithm.AES_GCM_NOPADDING, bodySMd5.getBytes(),
//						prometheusCodefCloudProperties.getAccessSecret().getBytes(), nonce.getBytes(),
//						timestamp.getBytes());
//				template.header(CodefHeader.HEADER_ACCESS_ID, prometheusCodefCloudProperties.getAccessId())
//						.header(CodefHeader.HEADER_AUTHENTICATION, authentication)
//						.header(CodefHeader.HEADER_NONCE, nonce)
//						.header(CodefHeader.HEADER_TENANTID_NAME, prometheusCodefCloudProperties.getTenantId())
//						.header(CodefHeader.HEADER_TIMESTAMP, timestamp).body(body, StandardCharsets.UTF_8);
//			} catch (JsonProcessingException e) {
//				throw new PrometheusException("this may not be happen", e);
//			}
//		}
//	}
//
//	public class CodefDecoder implements Decoder {
//
//		@Override
//		public Object decode(Response response, Type type) throws IOException, DecodeException, FeignException {
//			System.out.println(response.status());
//			return null;
//		}
//
//	}
//
//	public class CodefErrorDecoder implements ErrorDecoder {
//
//		@Override
//		public Exception decode(String methodKey, Response response) {
//			System.out.println(response.status());
//			System.out.println(response.headers());
//			System.out.println(response.body());
//			throw new PrometheusException("发送错误");
//		}
//
//	}
//
//}
